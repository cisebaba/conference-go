from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVO


# Declare a function to update the AccountVO object (ch, method, properties, body)
def process_updateAccountVO(ch, method, properties, body):
    content = json.loads(content)
    firstname = content["firstname"]
    lastname = content["lastname"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            firstname=firstname,
            lastname=lastname,
            email=email,
            is_active=is_active,
            update=updated,
        )
    else:
        AccountVO.objects.filter(email=email).delete()


while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange="account_info", queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=process_updateAccountVO,
            auto_ack=True,
        )
        print(" [*] Waiting for messages. To exit press CTRL+C")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
