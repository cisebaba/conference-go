import json

# import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(search_term):
    url = "https://api.pexels.com/v1/search?query=" + search_term
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    photos = data["photos"]

    return photos


def get_weather(city, state):
    url = (
        "http://api.openweathermap.org/geo/1.0/direct?q="
        + city
        + ","
        + state
        + ",US&limit=5&appid="
        + OPEN_WEATHER_API_KEY
    )
    response = requests.get(url)
    data = json.loads(response.content)[0]
    # print("url:", data)
    lon = str(data["lon"])
    lat = str(data["lat"])

    url = (
        "https://api.openweathermap.org/data/2.5/weather?lat="
        + lat
        + "&lon="
        + lon
        + "&appid="
        + OPEN_WEATHER_API_KEY
    )
    response2 = requests.get(url)
    data2 = json.loads(response2.content)
    weather = data2["weather"]
    main = data2["main"]

    return {"temp": main, "description": weather}
