import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    body = json.loads(body)
    send_mail(
        "Your presentation has been rejected!",
        body["presenter_name"]
        + " We're sadly to announce that your presentation "
        + body["title"]
        + " has been rejected",
        "admin@conference.go",
        [body["presenter_email"]],
        fail_silently=False,
    )


def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    body = json.loads(body)
    send_mail(
        "Your presentation has been approved!",
        body["presenter_name"]
        + " We're happy to announce that your presentation "
        + body["title"]
        + " has been approved",
        "admin@conference.go",
        [body["presenter_email"]],
        fail_silently=False,
    )


while True:
    try:
        # ALL OF YOUR CODE THAT HANDLES READING FROM THE
        # QUEUES AND SENDING EMAILS

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="project_rejection")
        channel.basic_consume(
            queue="project_rejection",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.queue_declare(queue="project_approvals")
        channel.basic_consume(
            queue="project_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        )
        print(" [*] Waiting for messages. To exit press CTRL+C")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)


# if __name__ == "__main__":
#     try:
#         main()
#     except KeyboardInterrupt:
#         print("Interrupted")
#         try:
#             sys.exit(0)
#         except SystemExit:
#             os._exit(0)
